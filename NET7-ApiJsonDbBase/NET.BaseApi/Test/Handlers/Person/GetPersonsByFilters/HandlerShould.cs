﻿using App.Core.Person.GetPersonsByFilters;
using App.Data.Person;
using App.Domain.Models.Person;
using AutoMapper;
using Moq;
using Test.Mothers.Person;
using Test.Repositories;
using Xunit;

namespace Test.Handlers.Person.GetPersonsByFilters;

public class HandlerShouldPerson : ServiceTest
{
    protected Mock<IMapper> MapperMock;
    protected Mock<IPersonRepository> PersonRepositoryServiceMock;
    protected GetPersonsByFiltersHandler Handler;

    public HandlerShouldPerson()
    {
        MapperMock = new Mock<IMapper>();
        PersonRepositoryServiceMock = PersonRepositoryServiceMockFactory.Default;
        Handler = new GetPersonsByFiltersHandler(PersonRepositoryServiceMock.Object, MapperMock.Object);
    }

    [Fact]
    public void ReturnThrowException_GetPersonsByFilters()
    {
        var persons = new List<PersonModel> { PersonMother.DefaultWithError };

        PersonRepositoryServiceMock.Setup(service => service
            .GetByFilters("Ale%", "M"))
            .ReturnsAsync(persons);

        var request = new GetPersonsByFiltersRequest()
        {
            Name = "Ale%",
            Gender = "M"
        };

        Assert.ThrowsAnyAsync<HttpRequestException>(() => Handler.Handle(request, CancellationToken.None));
    }

    [Fact]
    public async void ReturnData_GetPersonsByFilters()
    {
        var request = new GetPersonsByFiltersRequest()
        {
            Name = "Ale",
            Gender = "M"
        };

        var response = await PersonService.GetByFilters(request.Name, request.Gender);
        var responseData = response;
        Assert.NotEmpty(responseData);
    }
}
