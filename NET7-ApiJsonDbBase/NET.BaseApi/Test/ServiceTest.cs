﻿using App.Data;
using App.Data.Person;
using App.Domain.Models.Person;

namespace Test;

public class ServiceTest
{
    // Instance the dependences needly for handler tests
    protected PersonRepository PersonService;

    public ServiceTest()
    {
        PersonService = new PersonRepository(new JsonDbReader<IEnumerable<PersonModel>>());
    }
}