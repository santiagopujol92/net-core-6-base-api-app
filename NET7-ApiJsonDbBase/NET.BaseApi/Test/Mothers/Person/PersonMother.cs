﻿using App.Domain.Models.Person;

namespace Test.Mothers.Person
{
    public static class PersonMother
    {
        public static PersonModel Default => new PersonModel("Alejandro", "M");
        public static PersonModel DefaultWithError => new PersonModel("Ale%", "M");
    }
}