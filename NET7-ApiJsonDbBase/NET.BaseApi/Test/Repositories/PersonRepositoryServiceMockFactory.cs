﻿using App.Data.Person;
using App.Domain.Models.Person;
using Moq;

namespace Test.Repositories;

public static class PersonRepositoryServiceMockFactory
{
    public static Mock<IPersonRepository> Default
    {
        get
        {
            var mock = new Mock<IPersonRepository>();
            mock.Setup(service => service
                .GetByFilters(It.IsAny<string>(), It.IsAny<string>()))
                .ReturnsAsync(new List<PersonModel>());

            return mock;
        }
    }
}
