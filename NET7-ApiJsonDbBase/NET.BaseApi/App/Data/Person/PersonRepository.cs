﻿using App.Domain.Models.Person;

namespace App.Data.Person;

public class PersonRepository : IPersonRepository
{
    // This route config of file Json Db have to to stay in the root project(./App)
    // becouse need to be use by Test Project.
    // Normally this route should be a public route in the server
    private readonly string personsPathJsonDb = "./names.json"; 
    private readonly IJsonDbReader<IEnumerable<PersonModel>> _jsonDb;

    public PersonRepository(IJsonDbReader<IEnumerable<PersonModel>> jsonDb)
    {
        _jsonDb = jsonDb;
    }

    public async Task<IEnumerable<PersonModel>> GetAll()
    {
        return await _jsonDb.GetData(personsPathJsonDb);
    }

    public async Task<IEnumerable<PersonModel>> GetByFilters(string name, string gender)
    {
        var persons = await GetAll();
        return persons
            .Where(person => person.Gender == gender)
            .Where(person => person.Name.StartsWith(name));
    }
}