﻿using App.Domain.Models.Person;

namespace App.Data.Person;

public interface IPersonRepository
{
    Task<IEnumerable<PersonModel>> GetAll();

    Task<IEnumerable<PersonModel>> GetByFilters(string name, string gender);
}