﻿namespace App.Data;

public interface IJsonDbReader<T>
{
    Task<T> GetData(string pathJsonDb);
    //Task<T> AddData();
    //Task<T> UpdateData();
    //Task<T> DeleteData();
}