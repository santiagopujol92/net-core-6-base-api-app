﻿using App.Commons.Exceptions;
using Serilog;
using System.Text.Json;

namespace App.Data;

public class JsonDbReader<T> : IJsonDbReader<T>
{
    public async Task<T> GetData(string pathJsonDb)
    {
        try
        {
            using FileStream stream = File.OpenRead(pathJsonDb);
            var result = await JsonSerializer.DeserializeAsync<JsonBaseModel<T>>(stream);
            return result.Response;
        }
        catch (Exception ex)
        {
            Log.Error($"Error at read the json file in path '{pathJsonDb}'. | Error: {ex.Message}");
            throw new BadRequestApiException($"Error at read the json file in path '{pathJsonDb}'. | Error: {ex.Message}");
        }
    }
}