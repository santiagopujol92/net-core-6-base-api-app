﻿using System.Text.Json.Serialization;

namespace App.Data;

public class JsonBaseModel<T>
{
    [JsonPropertyName("response")]
    public T Response { get; set; }
}