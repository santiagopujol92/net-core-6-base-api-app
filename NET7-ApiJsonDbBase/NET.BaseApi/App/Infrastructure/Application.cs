﻿using App.Infrastructure.Extensions;
using App.Infrastructure.Middlewares;
using ElmahCore.Mvc;
using System.Reflection;

namespace App.Infrastructure;

public static class ApplicationExtensions
{
    public static WebApplicationBuilder ConfigApi(this WebApplicationBuilder builder)
    {
        builder.Services.AddHttpContextAccessor();
        builder.Services.AddSwagger();
        builder.Services.AddDatabaseRepositories();
        builder.Services.AddHandlers();
        builder.Services.AddValidators();
        builder.Services.AddAutoMappers();
        builder.Services.AddControllers();
        builder.Services.AddConfigErrorLogging(builder.Environment, Assembly.GetEntryAssembly()!.GetName().Name);
        return builder;
    }

    public static WebApplication CreateApp(this WebApplicationBuilder builder)
    {
        var app = builder.Build();
        app.UseSwagger();
        app.UseSwaggerUI();
        app.UseHttpsRedirection();
        app.UseAuthorization();
        app.UseApiExceptionHandler();
        app.UseHandledExceptionsMiddleware();
        app.UseErrorLogging();
        app.MapControllers();
        return app;
    }
}