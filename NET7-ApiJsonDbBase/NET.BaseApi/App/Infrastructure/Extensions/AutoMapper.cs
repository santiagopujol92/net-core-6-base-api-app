﻿using System.Reflection;

namespace App.Infrastructure.Extensions
{
    public static class AutoMapperExtensions
    {
        public static IServiceCollection AddAutoMappers(this IServiceCollection services)
        {
            services.AddAutoMapper(Assembly.Load("App"));
            return services;
        }
    }
}
