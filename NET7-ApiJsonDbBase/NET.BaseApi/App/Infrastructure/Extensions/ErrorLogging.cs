﻿using ElmahCore.Mvc;
using Microsoft.AspNetCore.Http.Features;

namespace App.Infrastructure.Extensions
{
    public static class ErrorLoggingExtensions
    {
        public static void AddConfigErrorLogging(this IServiceCollection services, IWebHostEnvironment env, string appName)
        {
            services.AddElmah(options =>
            {
                options.LogRequestBody = true;
                options.ApplicationName = appName;
                options.Path = "errorlog";
                options.OnPermissionCheck = context => context.User.Identity.IsAuthenticated;
            });
        }

        public static void UseErrorLogging(this IApplicationBuilder app)
        {
            app.UseWhen((HttpContext context) => context.Request.Path.StartsWithSegments("/errorlog", StringComparison.OrdinalIgnoreCase), delegate (IApplicationBuilder appBuilder)
            {
                appBuilder.Use(delegate (RequestDelegate next)
                {
                    RequestDelegate next2 = next;
                    return async delegate (HttpContext context)
                    {
                        context.Features.Get<IHttpBodyControlFeature>()!.AllowSynchronousIO = true;
                        await next2(context);
                    };
                });
            }).UseElmah();
        }
    }
}
