﻿using App.Commons.Handlers.Validators;
using MediatR;
using System.Reflection;

namespace App.Infrastructure.Extensions;

public static class MediatRExtensions
{
    public static IServiceCollection AddHandlers(this IServiceCollection services)
    {
        services.AddMediatR(cfg =>
        {
            cfg.RegisterServicesFromAssemblies(Assembly.Load("App"));
            cfg.AddBehavior(typeof(IPipelineBehavior<,>), typeof(ValidatorBehaviour<,>));
        });
        return services;
    }
}