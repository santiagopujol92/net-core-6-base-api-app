﻿using FluentValidation;
using System.Reflection;

namespace App.Infrastructure.Extensions;

public static class ValidatorsExtensions
{
    public static IServiceCollection AddValidators(this IServiceCollection services)
    {
        ValidatorOptions.Global.DefaultClassLevelCascadeMode = CascadeMode.Stop;
        services.AddValidatorsFromAssembly(Assembly.Load("App"));
        return services;
    }
}
