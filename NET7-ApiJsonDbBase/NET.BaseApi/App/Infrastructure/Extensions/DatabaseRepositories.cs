﻿using App.Data;
using App.Data.Person;
using App.Domain.Models.Person;namespace App.Infrastructure.Extensions;

public static class DatabaseRepositoriesExtensions
{
    public static IServiceCollection AddDatabaseRepositories(this IServiceCollection services)
    {
        //Add models references by Json Database
        services.AddScoped<IJsonDbReader<IEnumerable<PersonModel>>, JsonDbReader<IEnumerable<PersonModel>>>();

        //Add Repositories of models
        services.AddScoped<IPersonRepository, PersonRepository>();

        return services;
    }
}