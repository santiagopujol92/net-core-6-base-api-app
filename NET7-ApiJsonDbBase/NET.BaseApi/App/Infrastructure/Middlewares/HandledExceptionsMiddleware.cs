﻿using App.Commons.Api;
using App.Commons.Exceptions;
using FluentValidation;
using FluentValidation.Results;
using System.Net;
using System.Text;
using System.Text.Json;

namespace App.Infrastructure.Middlewares;

public static class HandledExceptionsMiddleware
{
    public static WebApplication UseHandledExceptionsMiddleware(this WebApplication app)
    {
        app.Use(async delegate (HttpContext context, Func<Task> next)
        {
            try
            {
                await next();
            }
            catch (ValidationException ex)
            {
                string text = JsonSerializer.Serialize(new BaseApiResponse<IEnumerable<object>>(ex.Errors.Select((ValidationFailure e) => new
                {
                    Code = e.ErrorCode,
                    Message = e.ErrorMessage
                }), new Message(HttpStatusCode.BadRequest, string.Join(". ", ex.Errors.Select((ValidationFailure e) => e.ErrorMessage)), "incorrect_validation")));
                context.Response.StatusCode = 400;
                context.Response.ContentType = "application/json";
                await context.Response.WriteAsync(text, Encoding.UTF8);
            }
            catch (ApiException ex2)
            {
                string text2 = JsonSerializer.Serialize(new BaseApiResponse<string>(ex2.Message, new Message(HttpStatusCode.BadRequest, ex2.Message, "Business validation error")));
                context.Response.StatusCode = 400;
                context.Response.ContentType = "application/json";
                await context.Response.WriteAsync(text2, Encoding.UTF8);
            }
        });
        return app;
    }
}
