﻿using App.Commons.Exceptions;
using Microsoft.AspNetCore.Diagnostics;
using Serilog;
using ElmahCore;
using System.Text;
using System.Text.Json;
using System.Net;

namespace App.Infrastructure.Middlewares
{
    public static class ExceptionHandlerMiddleware
    {
        public static void UseApiExceptionHandler(this IApplicationBuilder app)
        {
            app.UseExceptionHandler(delegate (IApplicationBuilder options)
            {
                options.Run(async delegate (HttpContext context)
                {
                    ApiException apiException = new ApiException((int)HttpStatusCode.InternalServerError, "A problem ocurr when process the request.", HttpStatusCode.InternalServerError.ToString(), "");
                    IExceptionHandlerFeature exceptionHandlerFeature = context.Features.Get<IExceptionHandlerFeature>();
                    if (exceptionHandlerFeature == null)
                    {
                        return;
                    }

                    if (exceptionHandlerFeature != null)
                    {
                        ApiException ex = exceptionHandlerFeature.Error as ApiException;
                        if (ex != null)
                        {
                            apiException = ex;
                            goto IL_00d4;
                        }
                    }
                    await context.RaiseError(exceptionHandlerFeature?.Error);
                    Log.Error($"An error has occurred in the system. {exceptionHandlerFeature?.Error.ToString()}");
                    goto IL_00d4;
                IL_00d4:
                    string text = JsonSerializer.Serialize(apiException.BaseApiResponse);
                    context.Response.StatusCode = apiException.StatusCode;
                    context.Response.ContentType = "application/json";
                    await context.Response.WriteAsync(text, Encoding.UTF8);
                });
            });
        }

        public static void UseExceptionHandlerMiddleware(this IApplicationBuilder app)
        {
            app.UseExceptionHandler(delegate (IApplicationBuilder options)
            {
                options.Run(async delegate (HttpContext context)
                {
                    IExceptionHandlerFeature exceptionHandlerFeature = context.Features.Get<IExceptionHandlerFeature>();
                    if (exceptionHandlerFeature != null)
                    {
                        Exception baseException = exceptionHandlerFeature.Error.GetBaseException();
                        string text;
                        if (baseException is ApiException)
                        {
                            text = baseException.Message ?? "";
                        }
                        else
                        {
                            await context.RaiseError(exceptionHandlerFeature.Error, delegate (HttpContext httpContext, Error excepcion)
                            {
                                return Task.CompletedTask;
                            });
                            text = "An error has occurred in the system.";
                        }

                        context.Response.StatusCode = 500;
                        context.Response.ContentType = "application/json";
                        await context.Response.WriteAsync(text, Encoding.UTF8);
                    }
                });
            });
        }
    }
}
