﻿using App.Domain.Models.Person;
using AutoMapper;

namespace App.Domain.Dtos.Person;

public class PersonProfile : Profile
{
    public PersonProfile()
    {
        CreateMap<PersonModel, PersonDto>();
    }
}