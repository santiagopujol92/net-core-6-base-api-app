﻿namespace App.Domain.Dtos.Person;

public class PersonDto
{
    public string Gender { get; set; }
    public string Name { get; set; }
}