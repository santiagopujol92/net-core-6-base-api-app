﻿using System.Text.Json.Serialization;

namespace App.Domain.Models.Person;

public class PersonModel
{
    [JsonPropertyName("gender")]
    public string Gender { get; private set; }

    [JsonPropertyName("name")]
    public string Name { get; private set; }

    public PersonModel(string name, string gender)
    {
        Gender = gender;
        Name = name;
    }
}
