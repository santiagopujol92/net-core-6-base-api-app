﻿using App.Commons.Api;
using App.Core.Person.GetPersonsByFilters;
using Microsoft.AspNetCore.Mvc;

namespace App.Controllers
{
    public class PersonsController : ApiControllerBase
    {
        /// <summary>
        /// Get persons by filters.
        /// </summary>
        /// <param name="name">Name string.</param>
        /// <param name="gender">Gender string</param>
        /// <returns>Person List</returns>
        /// <remarks>
        /// Example:
        ///      api/persons?name=Alejandro&gender=M
        /// </remarks>
        /// <response code="200">Success response.</response>
        /// <response code="204">Empty response.</response>
        /// <response code="400">Business validation error.</response>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(BaseApiResponse<GetPersonsByFiltersResponse>))]
        public async Task<IActionResult> GetPersonsByFilters(string name, string gender)
        {
            var result = await HandleRequestAsync(new GetPersonsByFiltersRequest()
            {
                Name = name,
                Gender = gender
            });

            return Ok(result.Persons);
        }
    }
}