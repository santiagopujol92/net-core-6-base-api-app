# Information Project

-Swagger para documentación e interfaz de la api.

-Mediator para controlar las request asyncronicas de forma mas eficiente y separar responsabilidades con las clases estándares 
(Request, RequestValidator, Handler, Response)

-Servicio de lector de archivos Json con configuración según el modelo de referencia de datos y la url de ubicación del mismo.

-Repositorios para manejar las operaciones de datos de entidades con modelos de referencia.

-Models y Dtos, haciendo referencia los models a las entidades de la base de datos, en este caso Personas. Y los Dtos haciendo referencia al mismo modelo trabajándose dentro de la aplicación(handlers) y para los responses.

-FluentValidations para las validaciones.

-Manejo de clases base custom para Excepciones con custom handlers y Mensajes para estandarizar respuestas.

-Clases base para los Controllers, para la ejecución de Mediator, y entre otras inyecciones necesarias a nivel global.

-AutoMapper para los mapeos entre Models y Dtos, respetando los Dtos para las respuestas de servicios y los modelos para la interacción con la base de datos.

-Serilog, para manejar y registrar errores de la aplicación en el log. 


Las capas layers serían:

-Commons: Biblioteca de clases base para todas las capas, en especial capa de Api y Core.

-Api: Aplicación .Net Core Api con Controllers.

-Core: Biblioteca de clases con lógica de negocio de servicios.

-Data: Biblioteca de clases con repositorios y contextos de base de datos.

-Domain: Biblioteca de clases con modelos y dtos.

-Infrastructure: Biblioteca de clases con configuración de inyeccion de dependencias del proyecto.

-Test: Console app UnitTest para tests de respuestas con handlers y repositorios.
