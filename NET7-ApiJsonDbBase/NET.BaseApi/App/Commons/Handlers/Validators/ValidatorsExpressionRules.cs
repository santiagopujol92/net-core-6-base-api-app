﻿namespace App.Commons.Handlers.Validators;

public static class ValidatorsExpressionRules
{
    public static readonly string XSSProtectedString = @"^[a-zA-Z0-9_áéíóúüÁÉÍÓÚÜñÑ,.!'?@ \\n]*$";
}