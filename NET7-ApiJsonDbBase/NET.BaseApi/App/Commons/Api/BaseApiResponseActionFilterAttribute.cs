﻿using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc;
using System.Net;

namespace App.Commons.Api;

public class BaseApiResponseActionFilterAttribute : ActionFilterAttribute
{
    public override void OnResultExecuting(ResultExecutingContext context)
    {
        OkObjectResult okObjectResult = context.Result as OkObjectResult;
        if (okObjectResult != null)
        {
            object value = okObjectResult.Value;
            Type type = value?.GetType() ?? typeof(object);
            if (!type.Name.Contains("BaseApiResponse"))
            {
                object obj2 = (okObjectResult.Value = Activator.CreateInstance(typeof(BaseApiResponse<>).MakeGenericType(type), value, new Message(HttpStatusCode.OK, "Execution Successfully.")));
            }
        }

        BadRequestObjectResult badRequestObjectResult = context.Result as BadRequestObjectResult;
        if (badRequestObjectResult != null)
        {
            badRequestObjectResult.Value = new BaseApiResponse<object>(new Message(HttpStatusCode.BadRequest, badRequestObjectResult.Value?.ToString()));
        }

        NotFoundObjectResult notFoundObjectResult = context.Result as NotFoundObjectResult;
        if (notFoundObjectResult != null)
        {
            notFoundObjectResult.Value = new BaseApiResponse<object>(new Message(HttpStatusCode.NotFound, notFoundObjectResult.Value?.ToString()));
        }

        base.OnResultExecuting(context);
    }
}