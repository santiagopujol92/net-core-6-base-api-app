﻿using App.Commons.Exceptions;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace App.Commons.Api;

[ApiController]
[Route("api/[controller]")]
[Produces("application/json", new string[] { })]
[TypeFilter(typeof(BaseApiResponseActionFilterAttribute))]
[ProducesResponseType(400, Type = typeof(BaseApiResponse<>))]
[ProducesResponseType(204, Type = typeof(BaseApiResponse<>))]
[ProducesResponseType(404, Type = typeof(BaseApiResponse<>))]
[ProducesResponseType(500, Type = typeof(BaseApiResponse<>))]
public class ApiControllerBase : Controller
{
    protected async Task<T> HandleRequestAsync<T>(IRequest<T> request)
    {
        IMediator service = base.HttpContext.RequestServices.GetService<IMediator>();
        CancellationToken requestAborted = base.HttpContext.RequestAborted;

        if (service == null) 
            throw new BadRequestApiException("Mediator service not exist.");

        return await service.Send(request, requestAborted);
    }
}