﻿using System.Text.Json.Serialization;

namespace App.Commons.Api;

public class BaseApiResponse<T>
{
    [JsonPropertyName("data")]
    public T Data { get; set; }

    [JsonPropertyName("messages")]
    public IList<Message> Messages { get; set; }

    public BaseApiResponse()
    {
    }

    public BaseApiResponse(T data)
    {
        Data = data;
    }

    public BaseApiResponse(Message message)
    {
        Messages = new List<Message> { message };
    }

    public BaseApiResponse(List<Message> messages)
    {
        Messages = messages;
    }

    public BaseApiResponse(T data, Message message)
    {
        Data = data;
        Messages = new List<Message> { message };
    }

    public BaseApiResponse(T data, string message)
    {
        Data = data;
        Messages = new List<Message>
        {
            new Message(message)
        };
    }
}