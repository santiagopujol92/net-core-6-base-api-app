﻿using Microsoft.AspNetCore.Http;
using System.Net;
using System.Text.Json.Serialization;

namespace App.Commons.Api;

/// <summary>
/// Represent an message occurr in application
/// </summary>
public class Message
{
    [JsonPropertyName("status")]
    public string Status { get; set; }

    [JsonPropertyName("code")]
    public int? StatusCode { get; set; }

    [JsonPropertyName("text")]
    public string Text { get; set; }

    [JsonPropertyName("help")]
    public string Help { get; set; }

    public Message()
    {
    }

    public Message(string text)
    {
        Text = text;
    }

    public Message(HttpStatusCode statusCode, string text)
    {
        StatusCode = (int)statusCode;
        Status = statusCode.ToString();
        Text = text;
    }

    public Message(HttpStatusCode statusCode, string text, string help)
    {
        StatusCode = (int)statusCode;
        Status = statusCode.ToString();
        Text = text;
        Help = help ?? string.Empty;
    }
}
