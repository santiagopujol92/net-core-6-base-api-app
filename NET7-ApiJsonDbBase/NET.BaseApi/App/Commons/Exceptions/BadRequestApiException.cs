﻿using App.Commons.Api;
using System.Net;

namespace App.Commons.Exceptions;

public class BadRequestApiException : ApiException
{
    public BadRequestApiException(string message)
        : base((int)HttpStatusCode.BadRequest, message)
    {
    }

    public BadRequestApiException(List<Message> messages)
        : base((int)HttpStatusCode.BadRequest, messages)
    {
    }

    public BadRequestApiException(string message, Exception exception)
        : base((int)HttpStatusCode.BadRequest, message, exception)
    {
    }

    public BadRequestApiException(string message, Exception exception, string code)
        : base((int)HttpStatusCode.BadRequest, message, exception, code)
    {
    }

    public BadRequestApiException(string message, Exception exception, string code, string help)
        : base((int)HttpStatusCode.BadRequest, message, exception, code, help)
    {
    }

    public BadRequestApiException(string message, string code, string help)
        : base((int)HttpStatusCode.BadRequest, message, code, help)
    {
    }
}
