using App.Infrastructure;

var builder = WebApplication.CreateBuilder(args);

builder.ConfigApi();

var app = builder.CreateApp();

app.Run();