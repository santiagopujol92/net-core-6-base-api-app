﻿using MediatR;
using System.Text.Json.Serialization;

namespace App.Core.Person.GetPersonsByFilters;

/// <summary>
/// Get persons by filters.
/// </summary>
/// <param name="name">Name string.</param>
/// <param name="gender">Gender string</param>
/// <returns>Person List</returns>
public class GetPersonsByFiltersRequest :IRequest<GetPersonsByFiltersResponse>
{
    public string Name { get; init; }

    public string Gender { get; init; }
}
