﻿using App.Commons.Handlers.Validators;
using FluentValidation;

namespace App.Core.Person.GetPersonsByFilters;

public class GetPersonsByFiltersRequestValidator : AbstractValidator<GetPersonsByFiltersRequest>
{
    public GetPersonsByFiltersRequestValidator()
    {
        RuleFor(request => request.Name)
            .NotEmpty()
            .Matches(ValidatorsExpressionRules.XSSProtectedString);

        RuleFor(request => request.Gender)
            .NotEmpty()
            .Length(1)
            .Matches(ValidatorsExpressionRules.XSSProtectedString);
    }
}