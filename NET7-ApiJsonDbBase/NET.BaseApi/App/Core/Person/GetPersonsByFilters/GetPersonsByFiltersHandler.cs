﻿using App.Data.Person;
using App.Domain.Dtos.Person;
using AutoMapper;
using MediatR;

namespace App.Core.Person.GetPersonsByFilters;

public class GetPersonsByFiltersHandler : IRequestHandler<GetPersonsByFiltersRequest, GetPersonsByFiltersResponse>
{
    private readonly IPersonRepository _personRepository;
    private readonly IMapper _mapper;

    public GetPersonsByFiltersHandler(IPersonRepository personRepository, IMapper mapper)
    {
        _personRepository = personRepository;
        _mapper = mapper;
    }

    public async Task<GetPersonsByFiltersResponse> Handle(GetPersonsByFiltersRequest request, CancellationToken cancellationToken)
    {
        var persons = await _personRepository.GetByFilters(request.Name, request.Gender);
        var personsDto = _mapper.Map<IEnumerable<PersonDto>>(persons);
        return new GetPersonsByFiltersResponse() { Persons = personsDto };
    }
}