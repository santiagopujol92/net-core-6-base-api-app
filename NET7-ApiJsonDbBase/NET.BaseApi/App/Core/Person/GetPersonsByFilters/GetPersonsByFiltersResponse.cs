﻿using App.Domain.Dtos.Person;

namespace App.Core.Person.GetPersonsByFilters;

public class GetPersonsByFiltersResponse
{
    public IEnumerable<PersonDto> Persons { get; set; }
}
