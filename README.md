# Technologies

### Swagger
### Mediator
### JsonReader
### MVC Entity Framework
### Models and Dtos.
### FluentValidations.
### Exceptions base
### Response Message base.
### AutoMapper
### Serilog

# Layers:

### Commons: Biblioteca de clases base para todas las capas, en especial capa de Api y Core.
### Api: Aplicación .Net Core Api con Controllers.
### Core: Biblioteca de clases con lógica de negocio de servicios.
### Data: Biblioteca de clases con repositorios y contextos de base de datos.
### Domain: Biblioteca de clases con Models y Dtos.
### Infrastructure: Biblioteca de clases con configuración de inyección de dependencias del proyecto.
### Test: Console app UnitTest para tests de respuestas con handlers y repositorios.
